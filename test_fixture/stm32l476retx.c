

/**
 * @brief This file is faking register set of a PIC32 MCU for the
 * 		  purposes of testing on host machine. Not all registers are
 * 		  defined in here. This list should grow with the testing
 * 		  application to meet the needs of the moment, as rewriting
 * 		  all the registers in one go is a very tedious and long process.
 */


#include "stm32l476xx.h"





