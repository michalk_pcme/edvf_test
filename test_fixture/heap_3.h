/*
 * heap_3.c
 *
 *  Created on: 27 Oct 2017
 *      Author: mike
 */

#ifndef TARGET_FAKE_INCLUDES_HEAP_3_C_
#define TARGET_FAKE_INCLUDES_HEAP_3_C_

/**
 * This is a fake file, so the Ceedling is going to link source *.c
 * file with corresponding name to the header. This is a workaround,
 * as Ceedling is very selective about which files get included in
 * the test.
 */

#endif /* TARGET_FAKE_INCLUDES_HEAP_3_C_ */
