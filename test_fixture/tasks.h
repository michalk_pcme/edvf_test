
#ifndef TEST_FIXTURE_TASKS_H_
#define TEST_FIXTURE_TASKS_H_

/**
 * This is a fake file, so the Ceedling is going to link source *.c
 * file with corresponding name to the header. This is a workaround,
 * as Ceedling is very selective about which files get included in
 * the test.
 */

#endif /* TEST_FIXTURE_TASKS_H_ */
