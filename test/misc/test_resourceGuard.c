
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>

#include "resourceGuard.h"

#include "mock_stm32l4xx_hal_spi.h"
#include "mock_systemDefinitions.h"

// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"


static int releaseCallCtr = 0;

static int masterHandle;
static rgDriver_t rgDriver;


void setUp(void)
{
	releaseCallCtr 	= 0;
}


void tearDown(void)
{
}


BaseType_t releaseSpiCallbackSuccess(uint32_t delay)
{
	releaseCallCtr++;

	rgUnregisterMaster(&rgDriver, &masterHandle);

	return pdTRUE;
}


BaseType_t releaseSpiCallbackNoRelease(uint32_t delay)
{
	releaseCallCtr++;

	//ownership does not get returned in this function
	//rgUnregisterMaster(&rgDriver, &masterHandle);

	return pdTRUE;
}


BaseType_t releaseSpiCallbackFailure(uint32_t delay)
{
	releaseCallCtr++;

	return pdFALSE;
}


//TODO Test the code for race conditions by playing with semaphores
//	   at various moments of code execution. This will require running
//	   separate test task in FreeRTOS.


/**
 * @brief rgDriverConstructor() test.
 * 	      Checks if fields are initialised to specific values.
 */
void test_Test01(void)
{
	rgDriver_t rgDriver;

	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_NULL(rgDriver.registeredMaster);
	TEST_ASSERT_NULL(rgDriver.releaseCallback);
}


/**
 * @brief rgRegisterNewMaster() test.
 * 	      Checks if function doesn't accept null pointers to driver, masterHandle and releaseCallback.
 */
void test_Test02(void)
{
	rgDriver_t* 	loc_driverHandle;
	void* 			loc_masterHandle;
	rgReleaseClb_t 	loc_callbackPtr;

	TEST_ASSERT_FALSE(rgRegisterNewMaster(0, 			    loc_masterHandle, loc_callbackPtr,0));
	TEST_ASSERT_FALSE(rgRegisterNewMaster(loc_driverHandle, 0, 			      loc_callbackPtr,0));
	TEST_ASSERT_FALSE(rgRegisterNewMaster(loc_driverHandle, loc_masterHandle, 0,0));
}


/**
 * @brief rgRegisterNewMaster() test.
 * 	      Checks if function registers new master.
 */
void test_Test03(void)
{
	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &masterHandle, &releaseSpiCallbackSuccess, 0));
}


/**
 * @brief rgRegisterNewMaster() test.
 * 	      Checks if function de-registers old master and registers new one.
 * 	      Checks if function triggers provided release callback on master re-registering.
 */
void test_Test04(void)
{
	int tempMasterHandle;

	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &masterHandle,     &releaseSpiCallbackSuccess, 0));
	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &tempMasterHandle, &releaseSpiCallbackSuccess, 0));
	TEST_ASSERT_EQUAL_INT64(1, releaseCallCtr);
}


/**
 * @brief rgRegisterNewMaster() test.
 * 	      Checks if function returns gracefully upon failed attempt to release the master ownership.
 */
void test_Test05(void)
{
	int tempMasterHandle;

	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE( rgRegisterNewMaster(&rgDriver, &masterHandle,     &releaseSpiCallbackNoRelease, 0));
	TEST_ASSERT_FALSE(rgRegisterNewMaster(&rgDriver, &tempMasterHandle, &releaseSpiCallbackSuccess,   0));
	TEST_ASSERT_EQUAL_INT64(1, releaseCallCtr);
}


/**
 * @brief rgRegisterNewMaster() test.
 * 	      Checks if function returns gracefully upon failed attempt to successfully trigger release callback
 */
void test_Test06(void)
{
	int tempMasterHandle;

	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE( rgRegisterNewMaster(&rgDriver, &masterHandle,     &releaseSpiCallbackFailure, 0));
	TEST_ASSERT_FALSE(rgRegisterNewMaster(&rgDriver, &tempMasterHandle, &releaseSpiCallbackSuccess, 0));
	TEST_ASSERT_EQUAL_INT64(1, releaseCallCtr);
}


/**
 * @brief rgUnregisterMaster() test.
 * 	      Checks if function unregisters master ownership upon valid master handle;
 */
void test_Test07(void)
{
	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &masterHandle, &releaseSpiCallbackSuccess, 0));

	rgUnregisterMaster(&rgDriver, &masterHandle);

	TEST_ASSERT_NULL(rgDriver.registeredMaster);
	TEST_ASSERT_NULL(rgDriver.releaseCallback);
}


/**
 * @brief rgUnregisterMaster() test.
 * 	      Checks if function does not unregister master ownership upon invalid master handle;
 */
void test_Test08(void)
{
	int tempMasterHandle;

	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &masterHandle, &releaseSpiCallbackSuccess, 0));

	rgUnregisterMaster(&rgDriver, &tempMasterHandle);

	if(&masterHandle != rgDriver.registeredMaster)
		TEST_ASSERT_TRUE(false);

	if(&releaseSpiCallbackSuccess != rgDriver.releaseCallback)
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief rgCheckMasterHandle() test.
 * 	      Checks if function returns false when no master holds ownership.
 * 	      Checks if function returns true if master holds ownership.
 */
void test_Test09(void)
{
	rgDriverConstructor(&rgDriver);

	TEST_ASSERT_FALSE(rgCheckMasterHandle(&rgDriver, &masterHandle));

	TEST_ASSERT_TRUE(rgRegisterNewMaster(&rgDriver, &masterHandle, &releaseSpiCallbackSuccess, 0));

	TEST_ASSERT_TRUE(rgCheckMasterHandle(&rgDriver, &masterHandle));
}




