
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>

#include "gainSwitches.h"

#include "mock_stm32l4xx_hal_gpio.h"
#include "mock_systemDefinitions.h"
#include "mock_systemInit.h"

#include "mock_measResGuard.h"

// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"



void setUp(void)
{
	HAL_GPIO_WritePin_Expect(GAIN_0_GPIO_Port, 		GAIN_0_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN_1_GPIO_Port, 		GAIN_1_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5L_GPIO_Port, 		GAIN5L_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5H_GPIO_Port, 		GAIN5H_Pin, 	GPIO_PIN_RESET);

	gsInitGainSwitches();
}


void tearDown(void)
{
}


void test_SetSwitches(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_GPIO_WritePin_Expect(GAIN_0_GPIO_Port, 		GAIN_0_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN_1_GPIO_Port, 		GAIN_1_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5L_GPIO_Port, 		GAIN5L_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN5H_GPIO_Port, 		GAIN5H_Pin, 	GPIO_PIN_RESET);

	uint16_t switchesState = 0;
	switchesState |= GS_SW_GAIN0_MSK;
	switchesState |= GS_SW_ROD_STATE_MSK;
	switchesState |= GS_SW_GAIN5L_MSK;
	gsSetSwitches(0, switchesState);
}


void test_SetSwitches_WrongMaster(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(false);

	uint16_t switchesState = 0;
	switchesState |= GS_SW_GAIN0_MSK;
	switchesState |= GS_SW_ROD_STATE_MSK;
	switchesState |= GS_SW_GAIN5L_MSK;
	gsSetSwitches(0, switchesState);
}


void test_SetSwitchesAux(void)
{
	HAL_GPIO_WritePin_Expect(GAIN_0_GPIO_Port, 		GAIN_0_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN_1_GPIO_Port, 		GAIN_1_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5L_GPIO_Port, 		GAIN5L_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN5H_GPIO_Port, 		GAIN5H_Pin, 	GPIO_PIN_RESET);

	uint16_t switchesState = 0;
	switchesState |= GS_SW_GAIN0_MSK;
	switchesState |= GS_SW_ROD_STATE_MSK;
	switchesState |= GS_SW_GAIN5L_MSK;
	gsSetSwitchesAux(switchesState);
}


void test_GetSwitchesStates(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_GPIO_WritePin_Expect(GAIN_0_GPIO_Port, 		GAIN_0_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN_1_GPIO_Port, 		GAIN_1_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5L_GPIO_Port, 		GAIN5L_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN5H_GPIO_Port, 		GAIN5H_Pin, 	GPIO_PIN_RESET);

	uint16_t switchesState = 0;
	switchesState |= GS_SW_GAIN0_MSK;
	switchesState |= GS_SW_ROD_STATE_MSK;
	switchesState |= GS_SW_GAIN5L_MSK;
	gsSetSwitches(0, switchesState);

	uint16_t readSwitchesState = 0;
	gsGetSwitches(&readSwitchesState);

	if(readSwitchesState != switchesState)
		TEST_ASSERT_TRUE(false);
}


void test_GetSingleSwitchState(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_GPIO_WritePin_Expect(GAIN_0_GPIO_Port, 		GAIN_0_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN_1_GPIO_Port, 		GAIN_1_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(ROD_STATE_GPIO_Port, 	ROD_STATE_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(OP_SELECT_GPIO_Port, 	OP_SELECT_Pin, 	GPIO_PIN_RESET);
	HAL_GPIO_WritePin_Expect(GAIN5L_GPIO_Port, 		GAIN5L_Pin, 	GPIO_PIN_SET);
	HAL_GPIO_WritePin_Expect(GAIN5H_GPIO_Port, 		GAIN5H_Pin, 	GPIO_PIN_RESET);

	//set switches to states
	uint16_t switchesState = 0;
	switchesState |= GS_SW_GAIN0_MSK;
	switchesState |= GS_SW_ROD_STATE_MSK;
	switchesState |= GS_SW_GAIN5L_MSK;
	gsSetSwitches(0, switchesState);

	//read single switch value
	bool switchState;

	switchState = false;
	gsGetSwitchState(GS_SW_GAIN0_MSK, &switchState);

	if(true != switchState)
		TEST_ASSERT_TRUE(false);

	switchState = false;
	gsGetSwitchState(GS_SW_GAIN1_MSK, &switchState);

	if(false != switchState)
		TEST_ASSERT_TRUE(false);
}


