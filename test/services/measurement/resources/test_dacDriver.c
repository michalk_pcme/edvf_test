
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>

#include "dacDriver.h"
#include "stm32l476xx.h"

#include "mock_systemInit.h"
#include "mock_systemDefinitions.h"
#include "mock_stm32l4xx_hal_dac.h"
#include "mock_stm32l4xx_hal_dma.h"
#include "mock_stm32l4xx_hal_gpio.h"
#include "mock_stm32l4xx_hal_cortex.h"
#include "mock_stm32l4xx_hal_tim.h"

#include "mock_acDmaAux.h"
#include "mock_acCommands.h"
#include "mock_measResGuard.h"


// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"


extern STATIC ddDriver_t ddDriver;

int mallocCtr;
int freeCtr;


void* CMOCK_safeMalloc_CALLBACK_FUNC(uint32_t cmock_arg1, int cmock_num_calls)
{
	mallocCtr++;
	return pvPortMalloc(cmock_arg1);
}


void CMOCK_safeFree_CALLBACK_FUNC(void* cmock_arg1, int cmock_num_calls)
{
	freeCtr++;
	vPortFree(cmock_arg1);
}


void setUp(void)
{
	mallocCtr 	= 0;
	freeCtr 	= 0;
}


void tearDown(void)
{
}


/**
 * @brief Check if interface is returning correct values after driver
 * 		  configuration.
 *
 * 		  Check if interface is returning false when no configuration
 * 		  mode has been selected
 */
void test_StartDcOutput(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_DC_OUTPUT, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(0x2000, ddDriver.dcOutput);
}


void test_StartDcOutput_WrongMaster(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(false);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_FALSE(ddStartDac(0, &dacConfig));

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_NONE, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(0, ddDriver.dcOutput);
}


void test_StartDcOutputAux(void)
{
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDacAux(&dacConfig));

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_DC_OUTPUT, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(0x2000, ddDriver.dcOutput);
}


/**
 * @brief Check if interface is returning correct values after driver
 * 		  configuration.
 *
 * 		  Check if interface is returning false when no configuration
 * 		  mode has been selected
 */
void test_StartSineWaveOutput(void)
{
	float     frequency;
	float     p_pAmplitude;
	float     dcOffset;

	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with Sine Wave configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_SINE_WAVE;
	dcOffset 	   = dacConfig.params.sineWave.dcOffset 	= 500;
	frequency 	   = dacConfig.params.sineWave.frequency 	= 25;
	p_pAmplitude   = dacConfig.params.sineWave.p_pAmplitude = 2500;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_SINE_WAVE, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(dcOffset,     ddDriver.dcOffset);
	TEST_ASSERT_EQUAL_UINT32(frequency,    ddDriver.frequency);
	TEST_ASSERT_EQUAL_UINT32(p_pAmplitude, ddDriver.p_pAmplitude);

	//this call is needed to release the heap memory assigned for sine wave buffer
	if(0 != ddDriver.sineBuffer)
		vPortFree(ddDriver.sineBuffer);
}


/**
 * @brief Check if interface is returning correct values after driver
 * 		  configuration.
 *
 * 		  Check if interface is returning false when no configuration
 * 		  mode has been selected
 */
void test_StartDacWithIncorrectSettings(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//incorrect initialisation should return false at the interface
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_NONE;
	TEST_ASSERT_FALSE(ddStartDac(0, &dacConfig));

	dacConfig.mode = DD_MODE_SINE_WAVE;
	dacConfig.params.sineWave.frequency	= DD_MIN_SINE_FREQ - 1;
	TEST_ASSERT_FALSE(ddStartDac(0, &dacConfig));

	dacConfig.mode = DD_MODE_SINE_WAVE;
	dacConfig.params.sineWave.frequency	= DD_MAX_SINE_FREQ + 1;
	TEST_ASSERT_FALSE(ddStartDac(0, &dacConfig));
}


/**
 * @brief Check if driver gets de-initialised after stopping.
 */
void test_StopDac(void)
{
	//start DAC
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//stop DAC
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Stop_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_DeInit_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//stop the DAC
	ddStopDac(0);

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_NONE, ddDriver.mode);
}


void test_StopDac_WrongMaster(void)
{
	//start DAC
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//stop DAC
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(false);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//stop the DAC
	ddStopDac(0);

	//check if driver fields did not get reset
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_DC_OUTPUT, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(0x2000,           ddDriver.dcOutput);
}


void test_StopDcOutputAux(void)
{
	//start DAC
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//stop DAC
	HAL_DAC_Stop_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_DeInit_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDacAux(&dacConfig));

	//stop the DAC
	ddStopDacAux();

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_NONE, ddDriver.mode);
}


/**
 * @brief Check if driver is reconfiguring itself on consecutive
 * 		  initialisations.
 */
void test_ReconfigurationOfDacOnAFly(void)
{
	//DAC start DC output
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//DAC stop DC output
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Stop_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_DeInit_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//DAC start Sine Wave output
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	//correct initialisation should return true at the interface
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//reconfigure the driver with Sine Wave configuration
	float     frequency;
	float     p_pAmplitude;
	float     dcOffset;
	dacConfig.mode = DD_MODE_SINE_WAVE;
	dcOffset 	   = dacConfig.params.sineWave.dcOffset 	= 500;
	frequency 	   = dacConfig.params.sineWave.frequency 	= 25;
	p_pAmplitude   = dacConfig.params.sineWave.p_pAmplitude = 2500;

	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//check if driver fields got configured properly
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_SINE_WAVE, ddDriver.mode);
	TEST_ASSERT_EQUAL_UINT32(dcOffset,     ddDriver.dcOffset);
	TEST_ASSERT_EQUAL_UINT32(frequency,    ddDriver.frequency);
	TEST_ASSERT_EQUAL_UINT32(p_pAmplitude, ddDriver.p_pAmplitude);

	//this call is needed to release the heap memory assigned for sine wave buffer
	if(0 != ddDriver.sineBuffer)
		vPortFree(ddDriver.sineBuffer);
}


/**
 * @brief Make sure that hardware configuration from HAL MSP is always
 * 		  done only on a DAC1 peripheral.
 */
void test_MspCallingWithIncorrectPeripheral(void)
{
	DAC_HandleTypeDef hdac;

	//place a wrong peripheral as an instance
	hdac.Instance =  (DAC_TypeDef*)TIM4;

	//this function should just return without calling any mocked interfaces
	ddDacDriver_HAL_DAC_MspInit(&hdac);
}


/**
 * @brief Tests sine wave mode reactions to faults in configuration.
 * 			- frequency too high
 *			- memory on heap not assigned
 */
extern STATIC bool ddStartSineWaveMode(ddConfig_t* config);

void test_SineWaveStartErrorHandling(void)
{
	ddConfig_t config;

	safeMalloc_IgnoreAndReturn(0);
	//safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_3);

	//init the driver
	ddInitDacDriver();

	config.mode = DD_MODE_SINE_WAVE;
	config.params.sineWave.dcOffset 	= 500;
	config.params.sineWave.p_pAmplitude = 2500;

	//frequency too low
	config.params.sineWave.frequency 	= DD_MIN_SINE_FREQ - 1;
	TEST_ASSERT_FALSE(ddStartSineWaveMode(&config));

	//frequency too high
	config.params.sineWave.frequency 	= DD_MAX_SINE_FREQ + 1;
	TEST_ASSERT_FALSE(ddStartSineWaveMode(&config));

	//sine wave buffer heap memory not assigned
	config.params.sineWave.frequency 	= 25;
	TEST_ASSERT_FALSE(ddStartSineWaveMode(&config));

	//this call is needed to release the heap memory assigned for sine wave buffer
	//sineBuffer should not be assigned to anything over here
	if(0 != ddDriver.sineBuffer)
	{
		TEST_ASSERT_TRUE(false);
	}
}


/**
 * @brief Tests if sine wave mode configuration code releases heap
 * 		  memory properly.
 */
void test_HeapMemoryRelease(void)
{
	float     frequency;
	float     p_pAmplitude;
	float     dcOffset;

	//mock calls from ddStartDac()
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//mock calls from ddStopDac()
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_TIM_Base_Stop_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_DeInit_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Stop_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_DeInit_ExpectAnyArgsAndReturn(HAL_OK);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);

	//init the driver
	ddInitDacDriver();

	//start the driver with Sine Wave configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_SINE_WAVE;
	dcOffset 	   = dacConfig.params.sineWave.dcOffset 	= 500;
	frequency 	   = dacConfig.params.sineWave.frequency 	= 25;
	p_pAmplitude   = dacConfig.params.sineWave.p_pAmplitude = 2500;

	//start the DAC - this should assign heap memory
	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//stop the DAC - this should release the heap memory
	ddStopDac(0);

	//check if buffer has been released (redirected to 0)
	if(0 != ddDriver.sineBuffer)
		TEST_ASSERT_TRUE(false);

	//check if number of calls to malloc and free was the same
	if(mallocCtr != freeCtr)
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief Checks correctness of generated sine wave samples.
 */
void test_SineWaveGenerationCorrectness(void)
{
	//mocks from ddStartSineWaveMode()
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_DMA_ExpectAnyArgsAndReturn(HAL_OK);

	//prepare the reference sine wave
	float deg;
	float dcOffset 	= (float)DD_DAC_MIDDLE_OFFSET;
	float amplitude = 1000;
	int bufCtr 		= 1/(25 * DD_MAX_DAC_UPDATE_PERIOD);
	float temp 		= (float)bufCtr;

	uint16_t refBuffer[bufCtr];

	for(int i=0; i<bufCtr; i++)
	{
		deg = (float)i * (float)(360.0/temp);

		refBuffer[i] = (uint16_t)(sin(deg * DD_PI/180) * amplitude + dcOffset);
	}

	//init the driver
	ddInitDacDriver();

	ddConfig_t config;
	config.mode = DD_MODE_SINE_WAVE;
	config.params.sineWave.dcOffset 	= 0;
	config.params.sineWave.p_pAmplitude = 2000;
	config.params.sineWave.frequency 	= 25;

	TEST_ASSERT_TRUE(ddStartSineWaveMode(&config));

	//compare driver's sine wave buffer with reference one
	for(int i=0; i<bufCtr; i++)
	{
		if(refBuffer[i] != ddDriver.sineBuffer[i])
			TEST_ASSERT_TRUE(false);
	}

	//this call is needed to release the heap memory assigned for sine wave buffer
	if(0 != ddDriver.sineBuffer)
		vPortFree(ddDriver.sineBuffer);
}


// settings are being sent to AUX on triggering ddSendSettingsToAux() function

/**
 * @brief This function should capture a DAC config data being sent upon triggering
 * 		  ddSendSettingsToAux() function.
 */
_Bool addMessageToQueue_Callback(acMessage_t* newMessage, uint32_t ticksToWait, int cmock_num_calls)
{
	if(0 != cmock_num_calls) return true;

	ddConfig_t* config;
	config = (ddConfig_t*)newMessage->paramPtr;

	TEST_ASSERT_EQUAL_UINT8(COMM_DD_SETTINGS_DATA, newMessage->command);
	TEST_ASSERT_EQUAL_UINT8(DD_MODE_DC_OUTPUT, config->mode);
	TEST_ASSERT_EQUAL_UINT32(0x2000, config->params.dcOutput.outputValue);

	return true;
}


void test_SendSettingsToAux(void)
{
	mrgCheckMasterOwnership_ExpectAnyArgsAndReturn(true);
	HAL_DAC_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_ConfigChannel_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_Start_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_DAC_SetValue_ExpectAnyArgsAndReturn(HAL_OK);
	addShortMessageToQueue_ExpectAnyArgsAndReturn(true);
	addMessageToQueue_StubWithCallback(&addMessageToQueue_Callback);

	//init the driver
	ddInitDacDriver();

	//start the driver with DC output configuration
	ddConfig_t dacConfig;
	dacConfig.mode = DD_MODE_DC_OUTPUT;
	dacConfig.params.dcOutput.outputValue = 0x2000;

	TEST_ASSERT_TRUE(ddStartDac(0, &dacConfig));

	//trigger a function to send settings over AUX
	ddSendSettingsToAux();
}



