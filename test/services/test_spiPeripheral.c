
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>

#include "spiPeripheral.h"
#include "mock_resourceGuard.h"

#include "mock_stm32l4xx_hal_spi.h"
#include "mock_systemDefinitions.h"



extern STATIC spDriver_t spDriver;

void* masterHandle;


void setUp(void)
{
}


void tearDown(void)
{
}


/**
 * @brief spInitSpiInstance() test.
 * 		  Check if initialisation follows specific procedure.
 */
void test_Test01(void)
{
	rgDriverConstructor_ExpectAnyArgs();

	spInitSpiInstance();

	if(SPI2 != spDriver.instance)
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief spGetMasterOwnership() test.
 * 		  Check if function returns true/false upon registering new master handle.
 */
void test_Test02(void)
{
	rgDriverConstructor_ExpectAnyArgs();
	rgRegisterNewMaster_ExpectAnyArgsAndReturn(true);
	rgRegisterNewMaster_ExpectAnyArgsAndReturn(false);

	spInitSpiInstance();

	TEST_ASSERT_TRUE(spGetMasterOwnership(masterHandle,0,0));
	TEST_ASSERT_FALSE(spGetMasterOwnership(masterHandle,0,0));
}


/**
 * @brief spReleaseMasterOwnership() test.
 * 		  Check if function follows specific procedure.
 */
void test_Test03(void)
{
	rgDriverConstructor_ExpectAnyArgs();
	rgUnregisterMaster_ExpectAnyArgs();

	spInitSpiInstance();

	spReleaseMasterOwnership(masterHandle);
}


/**
 * @brief spCheckMasterOwnership() test.
 * 		  Check if function returns true/false upon master check.
 */
void test_Test04(void)
{
	rgDriverConstructor_ExpectAnyArgs();
	rgCheckMasterHandle_ExpectAnyArgsAndReturn(true);
	rgCheckMasterHandle_ExpectAnyArgsAndReturn(false);

	spInitSpiInstance();

	TEST_ASSERT_TRUE( spCheckMasterOwnership(masterHandle));
	TEST_ASSERT_FALSE(spCheckMasterOwnership(masterHandle));
}


/**
 * @brief spGetSpiInstance() test.
 * 		  Check if function returns pointer to instance upon valid master check.
 * 		  Check if function returns pointer to 0 upon invalid master check.
 */
void test_Test05(void)
{
	rgDriverConstructor_ExpectAnyArgs();
	rgCheckMasterHandle_ExpectAnyArgsAndReturn(true);
	rgCheckMasterHandle_ExpectAnyArgsAndReturn(false);

	spInitSpiInstance();

	if(SPI2 != spGetSpiInstance(masterHandle))
		TEST_ASSERT_TRUE(false);

	if(0 != spGetSpiInstance(masterHandle))
		TEST_ASSERT_TRUE(false);
}




