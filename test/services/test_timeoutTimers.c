
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>

#include "timeoutTimers.h"

#include "mock_stm32l4xx_hal_tim.h"
#include "mock_systemDefinitions.h"


void setUp(void)
{
}


void tearDown(void)
{
}


void test_TimersInitialisedAsNonElapsed(void)
{
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_IT_ExpectAnyArgsAndReturn(HAL_OK);

	disableInterrupts_Ignore();
	enableInterrupts_Ignore();
	HAL_TIM_IRQHandler_Ignore();

	initTimeoutTimers();

	//software timer should not have elapsed yet
	if(true == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
		TEST_ASSERT_TRUE(false);

	TEST_ASSERT_TRUE(true);
}


void test_TimerUpdateFunctionality(void)
{
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_IT_ExpectAnyArgsAndReturn(HAL_OK);

	disableInterrupts_Ignore();
	enableInterrupts_Ignore();
	HAL_TIM_IRQHandler_Ignore();

	initTimeoutTimers();
	setTimeoutTimer(TT_RM_ZERO_CROSSING, 3);

	//emulate 2 x 1ms interrupts
	updateTimeoutTimers();
	updateTimeoutTimers();

	//software timer should not have elapsed yet
	if(true == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
		TEST_ASSERT_TRUE(false);

	//emulate another 1ms interrupts
	updateTimeoutTimers();

	//software timer should have elapsed now
	if(false == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
		TEST_ASSERT_TRUE(false);

	TEST_ASSERT_TRUE(true);
}


void test_TimerElapsesOnZeroPeriod(void)
{
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_IT_ExpectAnyArgsAndReturn(HAL_OK);

	disableInterrupts_Ignore();
	enableInterrupts_Ignore();
	HAL_TIM_IRQHandler_Ignore();

	initTimeoutTimers();
	setTimeoutTimer(TT_RM_ZERO_CROSSING, 0);

	//timer should be in elapsed state
	if(false == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
		TEST_ASSERT_TRUE(false);

	TEST_ASSERT_TRUE(true);
}


void test_TimerReset(void)
{
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_IT_ExpectAnyArgsAndReturn(HAL_OK);

	disableInterrupts_Ignore();
	enableInterrupts_Ignore();
	HAL_TIM_IRQHandler_Ignore();

	initTimeoutTimers();

	//timer should be in elapsed state after this setting
	setTimeoutTimer(TT_RM_ZERO_CROSSING, 0);

	//timer reset should take it from elapsed state
	resetTimeoutTimer(TT_RM_ZERO_CROSSING);

	//timer should not be in elapsed state
	if(true == hasTimeoutTimerElapsed(TT_RM_ZERO_CROSSING))
		TEST_ASSERT_TRUE(false);

	TEST_ASSERT_TRUE(true);
}


void test_CriticalSections(void)
{
	HAL_TIM_Base_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_ConfigClockSource_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIMEx_MasterConfigSynchronization_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_TIM_Base_Start_IT_ExpectAnyArgsAndReturn(HAL_OK);

	//this specifies exactly how critical sections should be created by:
	//	- setTimeoutTimer()
	//	- resetTimeoutTimer()
	disableInterrupts_Expect();
	enableInterrupts_Expect();
	disableInterrupts_Expect();
	enableInterrupts_Expect();

	HAL_TIM_IRQHandler_Ignore();

	initTimeoutTimers();
	setTimeoutTimer(TT_RM_ZERO_CROSSING, 0);
	resetTimeoutTimer(TT_RM_ZERO_CROSSING);

	TEST_ASSERT_TRUE(true);
}


