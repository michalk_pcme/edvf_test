/*
 * auxCommTestCommon.c
 *
 *  Created on: 26 Feb 2018
 *      Author: mike
 */


#include "acDmaAux.h"
#include "string.h"



acTxQueueItem_t createReferenceMessage(acMessage_t * newMessage, uint8_t* buffer, uint32_t bufSize)
{
	acTxQueueItem_t txItem;
	uint8_t crcVal;
	int i;

	memset(&txItem, 0, sizeof(acTxQueueItem_t));

	//assign memory for new DMA buffer frame
	txItem.size 	= (uint16_t)(newMessage->paramSize + AC_AUX_DMA_PROTOCOL_OVERHEAD);
	txItem.buffer 	= buffer;

	//assemble the DMA buffer frame
	txItem.buffer[0] = 'B';										//start bit
	txItem.buffer[1] = (uint8_t)(newMessage->paramSize + 1); 	//length = params + command
	txItem.buffer[2] = (uint8_t)newMessage->command;			//command

	memcpy(&txItem.buffer[3], newMessage->paramPtr, newMessage->paramSize);	//parameters

	//calculate CRC value
	crcVal = 0;
	for(i=0; i<txItem.size-1; i++)
	{
		crcVal = (uint8_t)(crcVal ^ txItem.buffer[i]);
	}

	txItem.buffer[txItem.size-1] = crcVal;						//CRC value

	return txItem;
}



