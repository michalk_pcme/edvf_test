
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>
#include <string.h>

#include "acDmaAux.h"
#include "stm32l476xx.h"

#include "mock_systemInit.h"
#include "mock_systemDefinitions.h"
#include "mock_stm32l4xx_hal_uart.h"
#include "mock_stm32l4xx_hal_dma.h"
#include "mock_stm32l4xx_hal_gpio.h"
#include "mock_stm32l4xx_hal_cortex.h"
//#include "mock_stm32l4xx_hal_tim.h"

#include "mock_acCommands.h"

#include "auxCommTestCommon.h"


// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"


#define REFERENCE_BUFFER_SIZE				255

extern STATIC acDmaAuxDriver_t acDriver;

int uartReceptionCtr;

templatePar_t templateVal;

volatile acTxQueueItem_t referenceMsg;
uint8_t referenceBuf[REFERENCE_BUFFER_SIZE];


volatile _Bool testReturnValue;
volatile int testFailPoint;


HAL_StatusTypeDef CMOCK_HAL_UART_Receive_DMA_CALLBACK_FUNC(UART_HandleTypeDef* huart, uint8_t* pData, uint16_t Size, int cmock_num_calls)
{
	uartReceptionCtr++;

	//process this function only on a first call
	if(1 != uartReceptionCtr)
		return HAL_OK;


	if(referenceMsg.size > Size)
	{
		TEST_ASSERT_TRUE(false);
	}
	else
	{
		//copy message data into DMA buffer
		memcpy(pData, referenceMsg.buffer, referenceMsg.size);

		//give sempahore to notify the task that message has been received
		xSemaphoreGive(acDriver.rxFinishedSemaphore);
	}

	return HAL_OK;
}


/**
 * @brief This function should be called from acCommands templateCommand()
 * 		  upon successful processing of the message pushed into the system
 * 		  in CMOCK_HAL_UART_Receive_DMA_CALLBACK_FUNC().
 */
_Bool CMOCK_accTriggerCommand_CALLBACK_FUNC(commandsIds cmdId, uint8_t* paramPtr, int cmock_num_calls)
{
	if(cmdId != COMM_TEMPLATE)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(&templateVal, paramPtr, sizeof(templatePar_t)))
		TEST_ASSERT_TRUE(false);

	return true;
}


void closeTheTest(_Bool loc_testReturnValue, int loc_testFailPoint)
{
	//return test result value to the testing framework
	testReturnValue = loc_testReturnValue;
	testFailPoint 	= loc_testFailPoint;

	//stop the scheduler
	vTaskEndScheduler();

	//just in case... wait for the world to end...
	while(1)
		asm("nop");
}


void setUp(void)
{
	uartReceptionCtr 	= 0;
}


void tearDown(void)
{
}


void taskTest( void* inPar )
{
	//give some time to other tasks to proceed..
	vTaskDelay(10/portTICK_PERIOD_MS);

	//check if HAL DMA UART reception has been triggered
	TEST_ASSERT_EQUAL_INT64(2, uartReceptionCtr);

	closeTheTest(true, 0);
}


/**
 * @brief This is a general functional test for UART reception. A fake message
 * 		  COMM_TEMPLATE with valid format gets generated and pushed into the
 * 		  AUX Comms reception system. Test is expecting a templateCommand()
 * 		  function to get triggered. Message parameters from this function
 * 		  are passed back to this test for comparison with original ones.
 */
void test_Test(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_Receive_DMA_StubWithCallback(&CMOCK_HAL_UART_Receive_DMA_CALLBACK_FUNC);
	HAL_UART_AbortReceive_ExpectAnyArgsAndReturn(HAL_OK);
	accTriggerCommand_StubWithCallback(&CMOCK_accTriggerCommand_CALLBACK_FUNC);

	testReturnValue = false;
	xTaskCreate( &taskTest, "taskTest", configMINIMAL_STACK_SIZE, NULL, AUX_COMMS_TASK_PRIORITY+1, NULL );

	//prepare reference raw template message (it is going to get pushed to
	//the reception in CMOCK_HAL_UART_Receive_DMA_CALLBACK_FUNC() )
	templateVal.valA = 0xDEADBEEF;
	templateVal.valB = 0xAB;
	templateVal.valC = 10.5;

	acMessage_t msg;
	msg.paramPtr  = (unsigned char*)&templateVal;
	msg.paramSize = sizeof(templatePar_t);
	msg.command   = COMM_TEMPLATE;

	referenceMsg = createReferenceMessage(&msg, referenceBuf, REFERENCE_BUFFER_SIZE);

	//init AUX Comms module
	acInitAuxDmaComms();

	//Set the scheduler running.  This function will not return unless a task calls vTaskEndScheduler(). */
	vTaskStartScheduler();

	//assert values
	if(true == testReturnValue)
	{
		TEST_ASSERT_TRUE(testReturnValue);
	}
	else
	{
		char message[255];
		snprintf(message, 255, "Test point fail: %d", testFailPoint);
		TEST_ASSERT_TRUE_MESSAGE(testReturnValue, message);
	}
}



