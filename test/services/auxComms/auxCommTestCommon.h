/*
 * auxCommTestCommon.h
 *
 *  Created on: 27 Feb 2018
 *      Author: mike
 */

#ifndef TEST_AUXCOMMS_AUXCOMMTESTCOMMON_H_
#define TEST_AUXCOMMS_AUXCOMMTESTCOMMON_H_


#include "acDmaAux.h"
#include "string.h"

acTxQueueItem_t createReferenceMessage(acMessage_t * newMessage, uint8_t* buffer, uint32_t bufSize);


#endif /* TEST_AUXCOMMS_AUXCOMMTESTCOMMON_H_ */
