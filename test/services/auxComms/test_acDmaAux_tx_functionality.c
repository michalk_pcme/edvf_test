
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>
#include <string.h>

#include "acDmaAux.h"
#include "stm32l476xx.h"

#include "mock_systemInit.h"
#include "mock_systemDefinitions.h"
#include "mock_stm32l4xx_hal_uart.h"
#include "mock_stm32l4xx_hal_dma.h"
#include "mock_stm32l4xx_hal_gpio.h"
#include "mock_stm32l4xx_hal_cortex.h"
//#include "mock_stm32l4xx_hal_tim.h"

#include "mock_acCommands.h"

#include "auxCommTestCommon.h"


// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"


#define CAPTURE_BUFFER_SIZE				255

extern STATIC acDmaAuxDriver_t acDriver;

int mallocCtr;
int freeCtr;
int uartTransmitCtr;

volatile acTxQueueItem_t capturedMsg;
volatile uint8_t capturedBuf[CAPTURE_BUFFER_SIZE];

volatile _Bool testReturnValue;
volatile int testFailPoint;



void* CMOCK_safeMalloc_CALLBACK_FUNC(uint32_t cmock_arg1, int cmock_num_calls)
{
	mallocCtr++;
	return pvPortMalloc(cmock_arg1);
}


void CMOCK_safeFree_CALLBACK_FUNC(void* cmock_arg1, int cmock_num_calls)
{
	freeCtr++;
	vPortFree(cmock_arg1);
}


HAL_StatusTypeDef CMOCK_HAL_UART_Transmit_DMA_CALLBACK_FUNC(UART_HandleTypeDef* huart, uint8_t* pData, uint16_t Size, int cmock_num_calls)
{
	uartTransmitCtr++;

	if(Size > CAPTURE_BUFFER_SIZE)
		TEST_ASSERT_TRUE(false);

	//copy the message to the capture buffer for further comparison
	capturedMsg.buffer = (uint8_t*)&capturedBuf[0];
	memcpy(capturedMsg.buffer, pData, Size);
	capturedMsg.size = Size;

	return HAL_OK;
}


void closeTheTest(_Bool loc_testReturnValue, int loc_testFailPoint)
{
	//return test result value to the testing framework
	testReturnValue = loc_testReturnValue;
	testFailPoint 	= loc_testFailPoint;

	//stop the scheduler
	vTaskEndScheduler();

	//just in case... wait for the world to end...
	while(1)
		asm("nop");
}


void setUp(void)
{
	mallocCtr 	= 0;
	freeCtr 	= 0;
	uartTransmitCtr = 0;
}


void tearDown(void)
{
}


void taskTest( void* inPar )
{
	acTxQueueItem_t refMsg;
	uint8_t buffer[CAPTURE_BUFFER_SIZE];

	//give some time to other tasks to proceed..
	vTaskDelay(10/portTICK_PERIOD_MS);

	templatePar_t templateVal;
	templateVal.valA = 0xDEADBEEF;
	templateVal.valB = 0xAB;
	templateVal.valC = 10.5;

	acMessage_t msg;
	msg.paramPtr  = (unsigned char*)&templateVal;
	msg.paramSize = sizeof(templatePar_t);
	msg.command   = COMM_TEMPLATE;

	//put message into the queue
	if(false == addMessageToQueue(&msg, MAX_DELAY))
		closeTheTest(false, 1);

	//create a reference message structure based on the same data
	refMsg = createReferenceMessage(&msg, buffer, CAPTURE_BUFFER_SIZE);

	//give some time to other tasks to proceed..
	vTaskDelay(10/portTICK_PERIOD_MS);

	//trigger DMA transfer finished callback
	HAL_UART_TxCpltCallback(&acDriver.huart);

	//give some time to other tasks to proceed..
	vTaskDelay(10/portTICK_PERIOD_MS);

	//memory for the message should have been allocated and released by now
	TEST_ASSERT_EQUAL_INT64(1, mallocCtr);
	TEST_ASSERT_EQUAL_INT64(1, freeCtr);

	//check if HAL DMA UART transmission has been triggered
	TEST_ASSERT_EQUAL_INT64(1, uartTransmitCtr);

	//check if captured message is same as a reference one
	if(0 != memcmp(refMsg.buffer, capturedMsg.buffer, refMsg.size))
		TEST_ASSERT_TRUE(false);

	TEST_ASSERT_EQUAL_UINT16(refMsg.size, capturedMsg.size);

	closeTheTest(true, 0);
}




/**
 * @brief This is a general functional test for UART transmission.
 * 		  This test is checking if message that is added to the Aux Comms
 * 		  queue gets processed properly and sent to the HAL DMA UART layer.
 */
void test_Test(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_Receive_DMA_ExpectAnyArgsAndReturn(HAL_OK);

	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	HAL_UART_Transmit_DMA_StubWithCallback(&CMOCK_HAL_UART_Transmit_DMA_CALLBACK_FUNC);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);

	testReturnValue = false;
	xTaskCreate( &taskTest, "taskTest", configMINIMAL_STACK_SIZE, NULL, AUX_COMMS_TASK_PRIORITY+1, NULL );

	acInitAuxDmaComms();

	//Set the scheduler running.  This function will not return unless a task calls vTaskEndScheduler(). */
	vTaskStartScheduler();

	//assert values
	if(true == testReturnValue)
	{
		TEST_ASSERT_TRUE(testReturnValue);
	}
	else
	{
		char message[255];
		snprintf(message, 255, "Test point fail: %d", testFailPoint);
		TEST_ASSERT_TRUE_MESSAGE(testReturnValue, message);
	}
}



