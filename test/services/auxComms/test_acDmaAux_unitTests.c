
#include "unity.h"
#include "UnityHelper.h"

#include <stdbool.h>
#include <string.h>

#include "acDmaAux.h"
#include "stm32l476xx.h"

#include "mock_systemInit.h"
#include "mock_systemDefinitions.h"
#include "mock_stm32l4xx_hal_uart.h"
#include "mock_stm32l4xx_hal_dma.h"
#include "mock_stm32l4xx_hal_gpio.h"
#include "mock_stm32l4xx_hal_cortex.h"
//#include "mock_stm32l4xx_hal_tim.h"

#include "mock_acCommands.h"
#include "auxCommTestCommon.h"


// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "croutine.h"
#include "deprecated_definitions.h"
#include "event_groups.h"
#include "list.h"
#include "mpu_wrappers.h"
#include "portable.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#include "tasks.h"
#include "port.h"
#include "heap_3.h"



extern STATIC acDmaAuxDriver_t acDriver;

extern STATIC void acAuxDmaHardwareInit(void);
extern STATIC void sendCommandUnknownMsg(void);
extern STATIC void sendTransmissionErrorMsg(void);
extern STATIC void acTransmitMessageOverDma(void);
extern STATIC void acProcessFinishedDmaRx(void);
extern STATIC bool acDmaAuxProcessRxBuffer(uint8_t* buffer, uint16_t bufferSize);
extern STATIC void acDmaAuxConfirmRx(void);



int mallocCtr;
int freeCtr;

templatePar_t test16_templateVal;


void* CMOCK_safeMalloc_CALLBACK_FUNC(uint32_t cmock_arg1, int cmock_num_calls)
{
	mallocCtr++;
	return pvPortMalloc(cmock_arg1);
}


void CMOCK_safeFree_CALLBACK_FUNC(void* cmock_arg1, int cmock_num_calls)
{
	freeCtr++;
	vPortFree(cmock_arg1);
}


_Bool CMOCK_accTriggerCommand_CALLBACK_FUNC(commandsIds cmdId, uint8_t* paramPtr, int cmock_num_calls)
{
	if(cmdId != COMM_TEMPLATE)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(&test16_templateVal, paramPtr, sizeof(templatePar_t)))
		TEST_ASSERT_TRUE(false);

	return true;
}



void setUp(void)
{
	mallocCtr 	= 0;
	freeCtr 	= 0;
}


void tearDown(void)
{
}



/**
 * @brief HAL_UART_TxCpltCallback() gives acDriver.txFinishedSemaphore.
 */
void test_Test01(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);

	acInitAuxDmaComms();

	//check the initial state of the txFinishedSemaphore
	if(pdFALSE != xSemaphoreTake(acDriver.txFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);

	//trigger the UART Tx Complete Callback interrupt function
	HAL_UART_TxCpltCallback(&acDriver.huart);

	//check the state of the txFinishedSemaphore
	if(pdTRUE != xSemaphoreTake(acDriver.txFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief HAL_UART_RxCpltCallback() triggers acDmaAuxConfirmRx.
 */
void test_Test02(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);

	acInitAuxDmaComms();

	//check the initial state of the rxFinishedSemaphore
	if(pdFALSE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);

	//trigger the interrupt function
	HAL_UART_RxCpltCallback(&acDriver.huart);

	//check the state of the rxFinishedSemaphore
	if(pdTRUE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief HAL_UART_ErrorCallback() triggers acDmaAuxConfirmRx
 */
void test_Test03(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);

	acInitAuxDmaComms();

	//check the initial state of the rxFinishedSemaphore
	if(pdFALSE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);

	//trigger the interrupt function
	HAL_UART_ErrorCallback(&acDriver.huart);

	//check the state of the rxFinishedSemaphore
	if(pdTRUE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief acDmaAuxUartIsr() triggers acDmaAuxConfirmRx upon detection of IDLE flag
 */
void test_Test04(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_IRQHandler_ExpectAnyArgs();
	HAL_UART_IRQHandler_ExpectAnyArgs();

	acInitAuxDmaComms();

	//check the initial state of the rxFinishedSemaphore
	if(pdFALSE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);

	//trigger the interrupt function
	acDmaAuxUartIsr();

	//state of rxFinishedSemaphore should not have changed from initial one
	if(pdFALSE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);

	//set the IDLE flag in mocked USART1 register
	acDriver.huart.Instance->ISR = USART_ISR_IDLE;

	//trigger the interrupt function
	acDmaAuxUartIsr();

	//state of rxFinishedSemaphore should have changed from initial one
	if(pdTRUE != xSemaphoreTake(acDriver.rxFinishedSemaphore, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief addMessageToQueue() generates a valid message
 */
void test_Test05(void)
{
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);

	acInitAuxDmaComms();

	templatePar_t templateVal;
	templateVal.valA = 0xDEADBEEF;
	templateVal.valB = 0xAB;
	templateVal.valC = 10.5;

	acMessage_t msg;
	msg.command   = COMM_TEMPLATE;
	msg.paramPtr  = (unsigned char*)&templateVal;
	msg.paramSize = sizeof(templatePar_t);

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//push a new message to the queue
	if(false == addMessageToQueue(&msg, 0))
		TEST_ASSERT_TRUE(false);

	//recover message from the queue
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}


/**
 * @brief addMessageToQueue() detects wrong length
 */
void test_Test06(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = AC_AUX_DMA_MAX_MESSAGE_LENGTH + 1;

	//push a new message to the queue
	if(false != addMessageToQueue(&msg, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief addMessageToQueue() recovers gracefully from the lack of heap memory
 */
void test_Test07(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_ExpectAnyArgsAndReturn(0);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//push a new message to the queue
	if(false != addMessageToQueue(&msg, 0))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief addMessageToQueue() releases heap when fails to put message to the queue
 */
void test_Test08(void)
{
	int i;
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//fill up whole queue with messages
	for(i=0; i<AC_AUX_DMA_TX_QUEUE_SIZE; i++){
		if(pdFALSE == xQueueSend(acDriver.txQueue, &refData, 0))
			TEST_ASSERT_TRUE(false);
	}

	//push a new message to the queue through the interface
	if(false != addMessageToQueue(&msg, 0))
		TEST_ASSERT_TRUE(false);

	//check number of times memory got allocated and released
	TEST_ASSERT_EQUAL_INT64(1, mallocCtr);
	TEST_ASSERT_EQUAL_INT64(1, freeCtr);
}


/**
 * @brief addShortMessageToQueue() generates a valid message with no parameters
 */
void test_Test09(void)
{
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//push a new message to the queue
	if(false == addShortMessageToQueue(COMM_NOTHING, 0))
		TEST_ASSERT_TRUE(false);

	//recover message from the queue
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	if(refData.size != AC_AUX_DMA_PROTOCOL_OVERHEAD)
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}


/**
 * @brief acAuxDma_HAL_UART_MspInit() initialises peripheral only for USART1
 */
void test_Test10(void)
{
	//assign wrong peripheral address to the instance
	acDriver.huart.Instance = (USART_TypeDef*)TIM4;

	//this function should just return without calling any mocked interfaces
	acAuxDma_HAL_UART_MspInit(&acDriver.huart);
}


/**
 * @brief acAuxDma_HAL_UART_MspDeInit() de-initialises peripheral only for USART1
 */
void test_Test11(void)
{
	//assign wrong peripheral address to the instance
	acDriver.huart.Instance = (USART_TypeDef*)TIM4;

	//this function should just return without calling any mocked interfaces
	acAuxDma_HAL_UART_MspDeInit(&acDriver.huart);
}


/**
 * @brief sendCommandUnknownMsg() generates a COMM_UNKNOWN_COMMAND message
 */
void test_Test12(void)
{
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_UNKNOWN_COMMAND;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//generate a COMM_UNKNOWN_COMMAND message
	sendCommandUnknownMsg();

	//recover message from the queue
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}


/**
 * @brief sendTransmissionErrorMsg() generates a COMM_TRANSMISSION_ERROR message
 */
void test_Test13(void)
{
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_TRANSMISSION_ERROR;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//generate a COMM_TRANSMISSION_ERROR message
	sendTransmissionErrorMsg();

	//recover message from the queue
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}


/**
 * @brief acDmaAuxTxTask send a message over DMA
 */
void test_Test14(void)
{
	acTxQueueItem_t txItem;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_Transmit_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	safeFree_ExpectAnyArgs();

	acInitAuxDmaComms();

	//emulate DMA transmission finished callback
	xSemaphoreGive(acDriver.txFinishedSemaphore);

	//start sending message
	acTransmitMessageOverDma();
}


/**
 * @brief acProcessFinishedDmaRx() swaps dmaRxBuffers upon reception
 */
void test_Test15(void)
{
	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_AbortReceive_ExpectAnyArgsAndReturn(HAL_OK);
	HAL_UART_Receive_DMA_ExpectAnyArgsAndReturn(HAL_OK);
	accTriggerCommand_ExpectAnyArgsAndReturn(true);

	acInitAuxDmaComms();

	//check if active DMA buffer is pointing somewhere
	if(acDriver.activeDmaBuffer != &acDriver.dmaRxBuffer1[0])
		TEST_ASSERT_TRUE(false);

	//create a message and put it into activeDmaBuffer
	//(to avoid processing of message frame errors)
	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//this function will write data into the activeDmaBuffer
	createReferenceMessage(&msg, acDriver.activeDmaBuffer, AC_AUX_DMA_RX_BUF_SIZE);

	//trigger processing of the DMA buffer
	acProcessFinishedDmaRx();

	//check if active DMA buffer got swapped to different one
	if(acDriver.activeDmaBuffer != &acDriver.dmaRxBuffer2[0])
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief acDmaAuxProcessRxBuffer() processes valid message correctly
 */
void test_Test16(void)
{
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	accTriggerCommand_StubWithCallback(&CMOCK_accTriggerCommand_CALLBACK_FUNC);

	acInitAuxDmaComms();

	test16_templateVal.valA = 0xDEADBEEF;
	test16_templateVal.valB = 0xAB;
	test16_templateVal.valC = 10.5;

	acMessage_t msg;
	msg.command   = COMM_TEMPLATE;
	msg.paramPtr  = (unsigned char*)&test16_templateVal;
	msg.paramSize = sizeof(templatePar_t);

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//this function should trigger CMOCK_accTriggerCommand_CALLBACK_FUNC()
	acDmaAuxProcessRxBuffer(refData.buffer, 255);
}


/**
 * @brief acDmaAuxProcessRxBuffer() detects wrong message upon invalid message format:
 * 			- starting byte
 *			- wrong length
 * 			- wrong CRC
 */
void test_Test17(void)
{
	int i;
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);

	acInitAuxDmaComms();

	templatePar_t templateVal;
	templateVal.valA = 0xDEADBEEF;
	templateVal.valB = 0xAB;
	templateVal.valC = 10.5;

	acMessage_t msg;
	msg.command   = COMM_TEMPLATE;
	msg.paramPtr  = (unsigned char*)&templateVal;
	msg.paramSize = sizeof(templatePar_t);

	//create a local reference message
	refData = createReferenceMessage(&msg, buffer, 255);

	//fill up whole queue with messages, so new error messages that are going
	//to get generated are not going to be added
	for(i=0; i<AC_AUX_DMA_TX_QUEUE_SIZE; i++){
		if(pdFALSE == xQueueSend(acDriver.txQueue, &refData, 0))
			TEST_ASSERT_TRUE(false);
	}

	//put wrong starting byte
	buffer[0] = 0;
	if(false != acDmaAuxProcessRxBuffer(refData.buffer, 255))
		TEST_ASSERT_TRUE(false);

	//put wrong length
	refData = createReferenceMessage(&msg, buffer, 255);
	buffer[1] = AC_AUX_DMA_PROTOCOL_OVERHEAD;
	if(false != acDmaAuxProcessRxBuffer(refData.buffer, 255))
		TEST_ASSERT_TRUE(false);

	//put wrong CRC value
	refData = createReferenceMessage(&msg, buffer, 255);
	int length = buffer[1];
	buffer[length+2] = 0;
	if(false != acDmaAuxProcessRxBuffer(refData.buffer, 255))
		TEST_ASSERT_TRUE(false);
}


/**
 * @brief acDmaAuxProcessRxBuffer() trigger sendTransmissionErrorMsg() upon
 * 		  wrong message format.
 */
void test_Test18(void)
{
	int i;
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_TEMPLATE;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local message so we could push it for RX processing
	refData = createReferenceMessage(&msg, buffer, 255);

	//put wrong starting byte, this should generate COMM_TRANSMISSION_ERROR
	buffer[0] = 0;
	if(false != acDmaAuxProcessRxBuffer(refData.buffer, 255))
		TEST_ASSERT_TRUE(false);

	//create a reference COMM_TRANSMISSION_ERROR message
	msg.command = COMM_TRANSMISSION_ERROR;
	refData = createReferenceMessage(&msg, buffer, 255);

	//recover message from the queue (it should be COMM_TRANSMISSION_ERROR)
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}


/**
 * @brief acDmaAuxProcessRxBuffer() triggers sendCommandUnknownMsg()
 * 		  upon unrecognised command ID
 */
void test_Test19(void)
{
	int i;
	acTxQueueItem_t refData;
	uint8_t buffer[255];

	HAL_UART_Init_ExpectAnyArgsAndReturn(HAL_OK);
	accTriggerCommand_ExpectAnyArgsAndReturn(false);
	safeMalloc_StubWithCallback(&CMOCK_safeMalloc_CALLBACK_FUNC);
	safeFree_StubWithCallback(&CMOCK_safeFree_CALLBACK_FUNC);

	acInitAuxDmaComms();

	acMessage_t msg;
	msg.command   = COMM_NOTHING;
	msg.paramPtr  = 0;
	msg.paramSize = 0;

	//create a local message so we could push it for RX processing
	refData = createReferenceMessage(&msg, buffer, 255);

	//put unrecognised command ID, this should generate COMM_UNKNOWN_COMMAND
	//buffer[2] = 0;
	if(false != acDmaAuxProcessRxBuffer(refData.buffer, 255))
		TEST_ASSERT_TRUE(false);

	//create a reference COMM_UNKNOWN_COMMAND message
	msg.command = COMM_UNKNOWN_COMMAND;
	refData = createReferenceMessage(&msg, buffer, 255);

	//recover message from the queue (it should be COMM_UNKNOWN_COMMAND)
	if(pdPASS != xQueueReceive(acDriver.txQueue, &acDriver.activeTxItem, 0))
		TEST_ASSERT_TRUE(false);

	//compare messages
	if(refData.size != acDriver.activeTxItem.size)
		TEST_ASSERT_TRUE(false);

	if(0 != memcmp(refData.buffer, acDriver.activeTxItem.buffer, refData.size))
		TEST_ASSERT_TRUE(false);

	//release the allocated memory
	if(0 != acDriver.activeTxItem.buffer)
		vPortFree(acDriver.activeTxItem.buffer);
}




